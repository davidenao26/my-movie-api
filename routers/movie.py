from fastapi import APIRouter, Depends, Path, Query
#fassAPI nos ayuda con la creacion de paginas web teniendo metodos GET, POST, PUT Y DELETE
#Body de FastAPI se utiliza para validar y procesar los datos enviados en una solicitud POST o PUT, asegurando que los datos cumplan con una estructura y un tipo de datos específicos.
#Path de fastAPI se utiliza para validar y procesar los datos enviados en unsa solicitud GET, asegurando que los datos cumplan con una estructura y un tipo de datos especificos
#Query de fastAPI se utiliza para validar y procesar los datos enviados en una solicitud GET de parametros query, asegurando que los datos cumplan con una estructura y un tipo de datos especificos
#status de fastAPI nos permite dar estados de los metodos GET POST PUT Y DELETE
from fastapi.responses import JSONResponse
# HtMLResponse se utiliza para enviar html como un return
# JSONResponse se utiliza para enviar json como un return
#BaseModel de pydantic me permite asignar los tipos de atributos de una clase y los tipos de los mismos
#Field  de pydantic me ayuda a validar los datos de estos atributos definidos con BaseModel
from typing import List, Optional
from pydantic import BaseModel, Field
#Optional de typing nos permite asignar la opcion de que un atributo de una clase sea opcional
#List de typing nos permite definir el tipo de lista de una respuesta 
# Importamos las variables de base de dato y la clase movie 
from config.database import Session
from models.movie import Movie as MovieModel
from middlewares.jwt_bearer import JWTBearer
from fastapi.encoders import jsonable_encoder
from services.movie import MovieServices
from schemas.movie import Movie

movie_router = APIRouter()



@movie_router.get('/movies', tags=['movies'], response_model=List[Movie], status_code=200, dependencies=[Depends(JWTBearer())])
def getmovies():
    db = Session()
    result = MovieServices(db).get_movies()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@movie_router.get('/movies/{id}', tags=['movies'], response_model=Movie)
def get_movie(id:int = Path(ge=1, le=2000)):
    db = Session()
    result = MovieServices(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={'message':' No encontrado'})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@movie_router.get('/movies/', tags=['movies'])
def get_movies_by_category(category:str = Query(min_length=5 , max_length=15)):
    db = Session()
    result = MovieServices(db).get_movie_by_category(category)
    if not result:
        return JSONResponse(status_code=404, content={'message':' No encontrado'})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))

@movie_router.post('/movies', tags=['movies'])
def create_movies(movie: Movie):
    db = Session()
    MovieServices(db).create_movie(movie)
    return JSONResponse(content={' ':'Se registro correctamente la pelicula'})


@movie_router.put('/movies/{id}', tags=['movies'], response_model=dict, status_code=200)
def update_movie(id:int, movie:Movie):
    db = Session()
    result = MovieServices(db).get_movie
    if not result:
        return JSONResponse(status_code=404, content={'message':' No encontrado'})
    MovieServices(db).update_movie(id, movie)
    return JSONResponse(status_code=200, content={'message':' Se ha moficado correctamente'})
        
@movie_router.delete('/movies/{id}', tags=['movies'])
def delete_movies(id:int):
    db = Session()
    result = MovieServices(db).delete_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={'message':' No encontrado'})
    return JSONResponse(status_code=200, content={'message':' Se ha eliminado la pelicula'})

