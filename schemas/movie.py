

from typing import Optional
from pydantic import BaseModel, Field


class Movie(BaseModel):
    id: Optional[int] = None
    title: str = Field(default='Mi pelicula', min_length=5 , max_length=15)
    overview: str
    year: int
    rating: float
    category: str
    # Cuando colocamos la clase config asigna valores por defecto a todos los atributos, por tanto ya no se tiene que escribir el 
    # default en el Field
    class Config():
        schema_extra = {
            'Example':{
                'id':1,
                'title':'Mi pelicula',
                'overview':'Descripcion de la pelicula',
                'year':2022,
                'rating':9.8,
                'category':'Accion'
            }
        }


