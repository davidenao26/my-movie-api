from fastapi import  FastAPI
#fassAPI nos ayuda con la creacion de paginas web teniendo metodos GET, POST, PUT Y DELETE
#Body de FastAPI se utiliza para validar y procesar los datos enviados en una solicitud POST o PUT, asegurando que los datos cumplan con una estructura y un tipo de datos específicos.
#Path de fastAPI se utiliza para validar y procesar los datos enviados en unsa solicitud GET, asegurando que los datos cumplan con una estructura y un tipo de datos especificos
#Query de fastAPI se utiliza para validar y procesar los datos enviados en una solicitud GET de parametros query, asegurando que los datos cumplan con una estructura y un tipo de datos especificos
#status de fastAPI nos permite dar estados de los metodos GET POST PUT Y DELETE
from fastapi.responses import HTMLResponse
# HtMLResponse se utiliza para enviar html como un return
# JSONResponse se utiliza para enviar json como un return
#BaseModel de pydantic me permite asignar los tipos de atributos de una clase y los tipos de los mismos
#Field  de pydantic me ayuda a validar los datos de estos atributos definidos con BaseModel
#Optional de typing nos permite asignar la opcion de que un atributo de una clase sea opcional
#List de typing nos permite definir el tipo de lista de una respuesta 
# Importamos las variables de base de dato y la clase movie 
from config.database import engine, Base
from middlewares.error_handler import ErrorHandler
from routers.movie import movie_router
from routers.auth import auth_router

app = FastAPI()
app.title = "Mi app con fastAPI"
app.version = "0.0.1"

app.add_middleware(ErrorHandler)
app.include_router(movie_router)
app.include_router(auth_router)

Base.metadata.create_all(bind = engine)

movies = [
    {
        'id':1,
        'title':'Avatar',
        'overview':"En un exhoberante planeta viven los Na'vi",
        'year':'2009',
        'rating':7.8,
        'category':'Accion'
    },
    {
        'id':2,
        'title':'Avatar',
        'overview':"En un exhoberante planeta viven los Na'vi",
        'year':'2009',
        'rating':7.8,
        'category':'Accion'
    }
]

@app.get('/', tags=['home'])
def mesage ():
    return HTMLResponse('<h1>hello world</h1>')



